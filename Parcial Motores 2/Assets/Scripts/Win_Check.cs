using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Win_Check : MonoBehaviour
{
    public GameObject txtGanaste;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            txtGanaste.SetActive(true);
            Time.timeScale = 0;
        }
    }
}
