using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Puerta : MonoBehaviour
{
    public Transform puerta;
    public Transform puertaAbierta;
    public Transform puertaCerrada;

    public bool candado = true;

    public float velocidadPuerta = 1f;
    Vector3 targetPosition;
    float time;

    private void Start()
    {
        targetPosition = puertaCerrada.position;
    }

    private void Update()
    {
        if(candado && puerta.position!= targetPosition)
        {
            puerta.transform.position = Vector3.Lerp(puerta.transform.position, targetPosition, time);
            time += Time.deltaTime * velocidadPuerta;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            targetPosition = puertaAbierta.position;
            time = 0;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            targetPosition = puertaCerrada.position;
            time = 0;
        }
    }

}
