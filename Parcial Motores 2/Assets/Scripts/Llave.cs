using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Llave : MonoBehaviour
{
    public Puerta abrirPuerta;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            abrirPuerta.candado = true;
        }

        Destroy(gameObject);
    }
}
